package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.EventPriority;
import de.paxii.clarinet.event.events.client.PostLoadModulesEvent;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.hud.renderables.ArmorHealthRenderable;
import de.paxii.clarinet.hud.renderables.HudRenderable;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

import java.util.HashMap;

public class ModuleHUD extends Module {

  private HashMap<String, HudRenderable> renderables;

  public ModuleHUD() {
    super("HUD", ModuleCategory.RENDER);

    this.setDescription("HUD, duh.");
    this.setVersion("1.0");
    this.setBuildVersion(19020);
    this.setRegistered(true);

    this.renderables = new HashMap<>();
    this.registerRenderable(new ArmorHealthRenderable());

    this.register();
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void onModulesLoaded(PostLoadModulesEvent loadModulesEvent) {
    this.renderables.values().forEach(renderable -> renderable.initialize(this));

    if (!this.isEnabled()) {
      this.unregister();
    }
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent tickEvent) {
    int posX = 0;
    int posY = Wrapper.getScaledResolution().getScaledHeight() - 10;

    for (HudRenderable renderable : this.renderables.values()) {
      if (!renderable.isEnabled()) {
        continue;
      }

      int height = renderable.render(posX, posY);

      posY -= height + 10;
    }
  }

  public boolean registerRenderable(HudRenderable renderable) {
    if (!this.renderables.containsKey(renderable.getName())) {
      this.renderables.put(renderable.getName(), renderable);

      return true;
    }

    return false;
  }

  public boolean removeRenderable(HudRenderable renderable) {
    if (this.renderables.containsKey(renderable.getName())) {
      this.renderables.remove(renderable.getName());

      return true;
    }

    return false;
  }
}
