package de.paxii.clarinet.hud.renderables;

import com.google.common.collect.Lists;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.module.external.ModuleHUD;
import de.paxii.clarinet.util.settings.type.ClientSettingBoolean;

import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Collections;

public class ArmorHealthRenderable implements HudRenderable {

  private ClientSettingBoolean enabled;

  @Override
  public void initialize(ModuleHUD module) {
    this.enabled = new ClientSettingBoolean(this.getName(), true);
    String settingKey = "render" + this.getName().replaceAll("\\p{javaSpaceChar}", "");
    module.getModuleSettings().put(settingKey, this.enabled);
  }

  @Override
  public String getName() {
    return "Armor Health";
  }

  @Override
  public int render(int posX, int posY) {
    posY -= 10;
    ArrayList<ItemStack> armorList = Lists.newArrayList(Wrapper.getPlayer().getArmorInventoryList());
    Collections.reverse(armorList);

    for (ItemStack itemStack : armorList) {
      if (itemStack == null || itemStack.getItem() == Item.getItemById(0)) {
        continue;
      }

      RenderHelper.enableGUIStandardItemLighting();
      Wrapper.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(itemStack, posX, posY);
      Wrapper.getMinecraft().getRenderItem().renderItemOverlayIntoGUI(Wrapper.getFontRenderer(), itemStack, posX, posY, null);
      RenderHelper.disableStandardItemLighting();
      posX += 20;
    }

    return 20;
  }

  @Override
  public boolean isEnabled() {
    return this.enabled.getValue();
  }

  @Override
  public void setEnabled(boolean enabled) {
    this.enabled.setValue(enabled);
  }
}
