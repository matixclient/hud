package de.paxii.clarinet.hud.renderables;

import de.paxii.clarinet.module.external.ModuleHUD;

public interface HudRenderable {

  void initialize(ModuleHUD module);

  String getName();

  /**
   * Render this component
   *
   * @param posX
   * @param posY
   * @return height this component needed
   */
  int render(int posX, int posY);

  boolean isEnabled();

  void setEnabled(boolean enabled);

}
